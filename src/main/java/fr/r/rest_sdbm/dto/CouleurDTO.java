package fr.r.rest_sdbm.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import fr.r.rest_sdbm.hateoas.HateOas;
import fr.r.rest_sdbm.metier.Couleur;
import jakarta.ws.rs.HttpMethod;
import jakarta.ws.rs.core.UriBuilder;

import java.util.ArrayList;
import java.util.List;

//TODO faire hériter la classe de HateOAS
public class CouleurDTO extends HateOas {
    //TODO ajouter les attributs de la classe Couleur
    @JsonProperty(index = 1)
    private Integer idCouleur;
    @JsonProperty(index = 2)
    private String nomCouleur;

    //constructeur DTO qui prend en paramètre une classe Couleur
    public CouleurDTO(Couleur couleur) {
        idCouleur = couleur.getId();
        nomCouleur = couleur.getLibelle();
    }

    //constructeur DTO qui prend en paramètre une classe Couleur et un URI
        //assigner valeur des attributs
        //creer et ajouter des liens grâce à l'URI pour les différentes méthodes (get, post, put, delete)
    public CouleurDTO(Couleur couleur, UriBuilder uriBuilder) {
        idCouleur = couleur.getId();
        nomCouleur = couleur.getLibelle();
        addLink("self", HttpMethod.GET, uriBuilder.clone().path(idCouleur.toString()).build());
        if (couleur.getNombreArticles() == 0)
            addLink("delete", HttpMethod.DELETE, uriBuilder.clone().path(idCouleur.toString()).build());
        Couleur parametre = new Couleur(0, "couleur");
        addLink("new", HttpMethod.POST, uriBuilder.clone().build(), parametre);
    }

    //méthode statique qui prend en paramètre une liste de couleurs et renvoie une liste d'objets CouleurDTO sans liste de liens
    public static List<CouleurDTO> toDtoList (List<Couleur> couleurs) {
        List<CouleurDTO> couleurDTOList = new ArrayList<>();
        for (Couleur couleur: couleurs) {
            CouleurDTO couleurDTO = new CouleurDTO(couleur);
            couleurDTOList.add(couleurDTO);
        }
        return couleurDTOList;
    }

    //méthode statique qui prend en paramètre une liste de couleurs et une URI et renvoie une liste d'objets CouleurDTO avec une liste de liens
    public static List<CouleurDTO> toDtoList (List<Couleur> couleurs, UriBuilder uriBuilder) {
        List<CouleurDTO> couleurDTOList = new ArrayList<>();
        for (Couleur couleur : couleurs) {
            CouleurDTO couleurDTO = new CouleurDTO(couleur, uriBuilder);
            couleurDTOList.add(couleurDTO);
        }
        return couleurDTOList;
    }

}
