package fr.r.rest_sdbm;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Response;

@Path("/hello")
public class HelloResource {
    @GET
    @Produces("text/plain")
    public String hello() {
        return "Hello, World!";
    }

    @GET
    @Path("/{name}")
    public Response hello(@PathParam("name") String s){
        System.out.printf("Hello, %s !", s);
        String result = "<h1>Hello " + s + " !</h1>";
        return Response.ok(result).build();
    }
}