package fr.r.rest_sdbm;


import fr.r.rest_sdbm.dao.DaoFactory;
import fr.r.rest_sdbm.dto.CouleurDTO;
import fr.r.rest_sdbm.hateoas.HateOas;
import fr.r.rest_sdbm.metier.Couleur;
import fr.r.rest_sdbm.security.Tokened;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.*;

import java.util.ArrayList;
import java.util.List;


// Ce service est accessible à l'url  "/couleurs"
@Tag(name = "Couleurs", description = "CRUD sur la table COULEUR")
@Path("couleurs")
@Produces(MediaType.APPLICATION_JSON)
public class CouleurResource {

    @Context
    UriInfo uriInfo;


    // Méthode appellée lors d'une requête HTTP GET
    @GET
    @Tokened
    @Operation(summary = "Liste des couleurs", description = "Renvoie la liste de toutes les couleurs.")
    @ApiResponse(responseCode = "200", description = "OK!")
    @ApiResponse(responseCode = "204", description = "Empty List!")
    public Response getAll() {
        List<Couleur> couleurs = DaoFactory.getCouleurDAO().getAll();
        /*return Response.ok(new GenericEntity<List<Couleur>>(couleurs) {
        }).build();*/

        List<CouleurDTO> couleurDTOList = CouleurDTO.toDtoList(couleurs, UriBuilder.fromUri(uriInfo.getBaseUriBuilder().path("couleurs").build()));
        return Response.status(Response.Status.OK).entity(couleurDTOList).build();
    }

    // Ce service est accessible à l'url  "/couleurs/id"
    @GET
    @Operation(summary = "Récupérer une couleur par son id", description = "Renvoie une couleur correspondant à l'id demandé.")
    @Path("{id}")
    public Response getById(@PathParam("id") Integer id) {

        Couleur couleur = DaoFactory.getCouleurDAO().getByID(id);
        if (couleur != null)
            return Response.ok(couleur).build();
        return Response.noContent().build();

    }

    @PUT
    @Operation(summary = "Modifier une couleur", description = "Modifier(update) une couleur correspondant à l'id demandé.")
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(@PathParam("id") Integer id, Couleur couleur) {

        if (couleur == null || id == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        if (id != couleur.getId()) {
            return Response.status(Response.Status.CONFLICT).entity(couleur).build();
        }
        if (DaoFactory.getCouleurDAO().update(couleur))
            //TODO utiliser la classe CouleurDTO
            //TODO renvoyer l'objet couleurDTO construit avec couleur plutôt que l'objet couleur directement
            return Response.ok(couleur).build();
        else
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response insert(Couleur couleur) {

        if (couleur == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        if (DaoFactory.getCouleurDAO().insert(couleur))
            return Response.ok(couleur).status(Response.Status.CREATED).build();
        else
            return Response.status(Response.Status.BAD_REQUEST).build();
    }

    @Tokened
    @DELETE
    @Consumes("application/json")
    @Path("{id}")
    public Response delete(@PathParam("id") Integer id) {

        if (id == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        if (DaoFactory.getCouleurDAO().delete(new Couleur(id, "")))
            return Response.status(204).build();
        else
            return Response.status(Response.Status.BAD_REQUEST).build();
    }
    
    @OPTIONS
    public Response options(){
        Couleur couleur = new Couleur(0, "couleur");
        HateOas hateOas = new HateOas();
        hateOas.addLink("All", HttpMethod.GET, uriInfo.getRequestUri());
        hateOas.addLink("By ID", HttpMethod.GET, uriInfo.getRequestUriBuilder().path("id").build());
        hateOas.addLink("New", HttpMethod.POST, uriInfo.getRequestUri(), "Couleur");
        return Response.ok(hateOas).build();
    }
}