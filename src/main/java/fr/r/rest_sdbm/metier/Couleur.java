package fr.r.rest_sdbm.metier;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fr.r.rest_sdbm.dao.DaoFactory;
import io.swagger.v3.oas.annotations.media.Schema;

import java.util.Objects;

public class Couleur {
    @Schema(required = true, example = "0")
    private int id;
    @Schema(required = true, example = "Rose")
    private String libelle;
    @JsonIgnore
    private int nombreArticles;

    public Couleur() {
        id = 0;
        libelle = "";
    }
    public Couleur(int id, String nomCouleur) {
        this.id = id;
        this.libelle = nomCouleur;
        if (this.id == 0)
            this.nombreArticles = 0;
        else
            this.nombreArticles = DaoFactory.getCouleurDAO().countArticles(this);
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getLibelle() {
        return libelle;
    }
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public int getNombreArticles() {
        return nombreArticles;
    }

    public void setNombreArticles(int nombreArticles) {
        this.nombreArticles = nombreArticles;
    }

    @Override
    public String toString() {
        return libelle;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Couleur couleur = (Couleur) object;
        return Objects.equals(id, couleur.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
