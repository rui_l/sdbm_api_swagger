package fr.r.rest_sdbm.metier;

public class Continent {
    private Integer id;
    private String libelle;

    public Continent() {
        id = 0;
        libelle = "";
    }
    public Continent(int id, String nomContinent) {
        this.id = id;
        this.libelle = nomContinent;
    }

    public int getId() {
       return this.id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getLibelle() {
        return this.libelle;
    }
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Override
    public String toString() {
        return libelle;
    }

}
