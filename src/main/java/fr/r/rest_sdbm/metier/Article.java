package fr.r.rest_sdbm.metier;

public class Article {
    private Integer id;
    private String libelle;
    private float prixAchat;
    private Integer volume;
    private Float titrage;
    private Marque marque;
    private Couleur couleur;
    private TypeBiere typeBiere;

    public Article() {
        this.marque = new Marque();
        this.couleur = new Couleur();
        this.typeBiere = new TypeBiere();
    }

    public Article(int id, String nom, int volume, float titrage) {
        this.id = id;
        this.libelle = nom;
        this.volume = volume;
        this.titrage = titrage;
        this.marque = new Marque();
        this.couleur = new Couleur();
        this.typeBiere = new TypeBiere();
    }

    public Article(int id, String nom, float prixAchat, int volume, float titrage) {
        this.id = id;
        this.libelle = nom;
        this.prixAchat = prixAchat;
        this.volume = volume;
        this.titrage = titrage;
    }

    /*on ne fait pas de constructeur avec autant de paramètres !!!*/
    public Article(int id, String nom, float prixAchat, int volume, float titrage, Marque marque, Couleur couleur, TypeBiere typeBiere) {
        this.id = id;
        this.libelle = nom;
        this.prixAchat = prixAchat;
        this.volume = volume;
        this.titrage = titrage;
        this.marque = marque;
        this.couleur = couleur;
        this.typeBiere = typeBiere;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public float getPrixAchat() {
        return prixAchat;
    }

    public void setPrixAchat(float prixAchat) {
        this.prixAchat = prixAchat;
    }

    public Integer getVolume() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public Float getTitrage() {
        return titrage;
    }

    public void setTitrage(Float titrage) {
        this.titrage = titrage;
    }

    public Marque getMarque() {
        return marque;
    }

    public void setMarque(Marque marque) {
        this.marque = marque;
    }

    public Couleur getCouleur() {
        return couleur;
    }

    public void setCouleur(Couleur couleur) {
        this.couleur = couleur;
    }

    public TypeBiere getTypeBiere() {
        return typeBiere;
    }

    public void setTypeBiere(TypeBiere typeBiere) {
        this.typeBiere = typeBiere;
    }

    @Override
    public String toString() {
        return this.libelle;
    }
}
