package fr.r.rest_sdbm.hateoas;


import com.fasterxml.jackson.annotation.JsonInclude;

import java.net.URI;

public class Link {
    private String name;
    private String method;
    private URI uri;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Object parameter;

    public Link(String name, String method, URI uri) {
        this.name = name;
        this.method = method;
        this.uri = uri;
    }

    public Link(String name, String method, URI uri, Object parameter) {
        this.name = name;
        this.method = method;
        this.uri = uri;
        this.parameter = parameter;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public URI getUri() {
        return uri;
    }

    public void setUri(URI uri) {
        this.uri = uri;
    }

    public Object getParameter() {
        return parameter;
    }

    public void setParameter(Object parameter) {
        this.parameter = parameter;
    }
}
