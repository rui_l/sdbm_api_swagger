package fr.r.rest_sdbm.hateoas;

import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class HateOas implements Serializable {
    private List<Link> links = new ArrayList<>();

    public List<Link> getLinks() {
        return links;
    }

    public void addLink(String name, String httpMethod, URI uri) {
        Link link = new Link(name, httpMethod, uri);
        links.add(link);
    }

    public void addLink(String name, String httpMethod, URI uri, Object object) {
        Link link = new Link(name, httpMethod, uri, object);
        links.add(link);
    }
}
