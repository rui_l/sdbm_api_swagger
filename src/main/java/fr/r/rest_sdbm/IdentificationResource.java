package fr.r.rest_sdbm;

import fr.r.rest_sdbm.hateoas.HateOas;
import fr.r.rest_sdbm.security.MyToken;
import fr.r.rest_sdbm.security.User;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.ws.rs.HttpMethod;
import jakarta.ws.rs.core.*;

import java.net.URI;

@Tag(name = "Authentication")
public class IdentificationResource {
    @Context
    UriInfo uriInfo;

    public Response insert(User user) {
        if (user == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        if (user.getLogin().equals("MyLogin") && user.getPassword().equals("MyPassport")) {
            return Response.ok().header(HttpHeaders.AUTHORIZATION, MyToken.generate(user)).build();
        }
        UriBuilder uriBuilder = UriBuilder.fromUri(uriInfo.getRequestUri());
        URI forgetPasswordURI = uriBuilder.clone().path("forgetPassword").queryParam("Login", user.getLogin()).build();
        URI forgetLoginURI = uriBuilder.clone().path("forgetLogin").queryParam("email", "xxx").build();

        HateOas hateOas = new HateOas();
        hateOas.addLink("Forget Password", HttpMethod.GET, forgetPasswordURI);
        hateOas.addLink("Forget Login", HttpMethod.GET, forgetLoginURI);
        return Response.ok().status(Response.Status.UNAUTHORIZED).build();
    }
}
