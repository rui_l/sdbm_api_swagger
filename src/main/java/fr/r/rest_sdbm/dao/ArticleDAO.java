package fr.r.rest_sdbm.dao;



import fr.r.rest_sdbm.metier.Article;
import fr.r.rest_sdbm.metier.ArticleSearch;

import java.sql.*;
import java.util.ArrayList;

public class ArticleDAO extends DAO<Article, ArticleSearch, Integer> {
    @Override
    public Article getByID(Integer id) {
        String sqlRequest = "SELECT * FROM ARTICLE where ID_ARTICLE=?";
        try (PreparedStatement preparedStatement = connexion.prepareStatement(sqlRequest, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setInt(1,id);
            ResultSet resultSet=preparedStatement.executeQuery();
            if (resultSet.next()) {
                Article article = new Article(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getFloat(3),
                        resultSet.getInt(4),
                        resultSet.getFloat(5)
                );
                if (resultSet.getInt(6) !=0) {
                    article.setMarque(DaoFactory.getMarqueDAO().getByID(resultSet.getInt(6)));
                }
                Integer idCouleur = resultSet.getInt(7);
                if (idCouleur !=null && idCouleur !=0) {
                    article.setCouleur(DaoFactory.getCouleurDAO().getByID(idCouleur));
                }
                Integer idType = resultSet.getInt(8);
                if (idType !=null && idType !=0) {
                    article.setTypeBiere(DaoFactory.getTypeDAO().getByID(idType));
                }
                return article;
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<Article> getAll() {
        ArrayList<Article> liste = new ArrayList<>();
        String sqlRequest = "SELECT ID_ARTICLE, NOM_ARTICLE, PRIX_ACHAT, VOLUME, TITRAGE, ID_MARQUE, ID_COULEUR, ID_TYPE from ARTICLE";
        try (Statement statement = connexion.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sqlRequest);
            while (resultSet.next()) {
                Article article = new Article(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getFloat(3),
                        resultSet.getInt(4),
                        resultSet.getFloat(5));
                if (resultSet.getInt(6) !=0) {
                    article.setMarque(DaoFactory.getMarqueDAO().getByID(resultSet.getInt(6)));
                }
                Integer idCouleur = resultSet.getInt(7);
                if (idCouleur !=null && idCouleur !=0) {
                    article.setCouleur(DaoFactory.getCouleurDAO().getByID(idCouleur));
                }
                Integer idType = resultSet.getInt(8);
                if (idType !=null && idType !=0) {
                    article.setTypeBiere(DaoFactory.getTypeDAO().getByID(idType));
                }
                liste.add(article);
            }
            resultSet.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return liste;
    }

    @Override
    public ArrayList<Article> getLike(ArticleSearch articleSearch) {
        ResultSet resultSet;
        ArrayList<Article> liste = new ArrayList<>();
        String procedureStockee =
                "{call PS_QBE_VUE_ARTICLE(?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        try(CallableStatement callableStatement = this.connexion.prepareCall(procedureStockee)) {
            callableStatement.setString(1,articleSearch.getLibelle());
            callableStatement.setInt(2,articleSearch.getVolume());
            callableStatement.setFloat(3,articleSearch.getTitrageMin());
            callableStatement.setFloat(4,articleSearch.getTitrageMax());
            callableStatement.setInt(5,articleSearch.getMarque().getId());
            callableStatement.setInt(6,articleSearch.getFabricant().getId());
            callableStatement.setInt(7,articleSearch.getPays().getId());
            callableStatement.setInt(8,articleSearch.getContinent().getId());
            callableStatement.setInt(9,articleSearch.getCouleur().getId());
            callableStatement.setInt(10,articleSearch.getTypeBiere().getId());
            callableStatement.setNull(11, Types.INTEGER);
            callableStatement.setNull(12,Types.INTEGER);
            callableStatement.registerOutParameter(13,Types.INTEGER);

            resultSet = callableStatement.executeQuery();
            while (resultSet.next()) {
                Article article = new Article(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getFloat(3),
                        resultSet.getInt(4),
                        resultSet.getFloat(5)
                );
                if (resultSet.getInt(6) !=0) {
                    article.setMarque(DaoFactory.getMarqueDAO().getByID(resultSet.getInt(6)));
                }
                Integer idCouleur = resultSet.getInt(14);
                if (idCouleur !=null && idCouleur !=0) {
                    article.setCouleur(DaoFactory.getCouleurDAO().getByID(idCouleur));
                }
                Integer idType = resultSet.getInt(16);
                if (idType !=null && idType !=0) {
                    article.setTypeBiere(DaoFactory.getTypeDAO().getByID(idType));
                }
                liste.add(article);
            }
            int nombreDeLignesTotales = callableStatement.getInt(13);
            articleSearch.setNbTotalDeLignes(nombreDeLignesTotales);
            resultSet.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return liste;
    }

    @Override
    public boolean insert(Article objet) {
        return false;
    }

    @Override
    public boolean update(Article object) {
        return false;
    }

    @Override
    public boolean delete(Article object) {
        return false;
    }
}
