package fr.r.rest_sdbm.dao;

import fr.r.rest_sdbm.metier.Continent;
import fr.r.rest_sdbm.metier.Pays;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class PaysDAO extends DAO<Pays, Pays, Integer> {
    @Override
    public Pays getByID(Integer id) {
        String sqlRequest = "SELECT ID_PAYS, NOM_PAYS, ID_CONTINENT FROM PAYS WHERE ID_PAYS=?";
        try (PreparedStatement preparedStatement = connexion.prepareStatement(sqlRequest, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return new Pays(resultSet.getInt(1), resultSet.getString(2), DaoFactory.getContinentDAO().getByID(resultSet.getInt(3)));
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<Pays> getAll() {
        ArrayList<Pays> liste = new ArrayList<>();
        String sqlRequest = "SELECT ID_PAYS, NOM_PAYS, C.ID_CONTINENT, NOM_CONTINENT FROM PAYS P JOIN CONTINENT C ON P.ID_CONTINENT = C.ID_CONTINENT";
        try (PreparedStatement preparedStatement=connexion.prepareStatement(sqlRequest, Statement.RETURN_GENERATED_KEYS)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                liste.add(new Pays(resultSet.getInt(1), resultSet.getString(2), new Continent(resultSet.getInt(3), resultSet.getString(4))));
            }
            resultSet.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return liste;
    }

    @Override
    public ArrayList<Pays> getLike(Pays objet) {
        ArrayList<Pays> liste = new ArrayList<>();
        String sqlRequest = "SELECT ID_PAYS, NOM_PAYS, ID_CONTINENT FROM PAYS WHERE NOM_PAYS LIKE '%" +objet.getLibelle() + "%'";
        try (PreparedStatement preparedStatement=connexion.prepareStatement(sqlRequest, Statement.RETURN_GENERATED_KEYS)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                liste.add(new Pays(resultSet.getInt(1), resultSet.getString(2)));
            }
            resultSet.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return liste;
    }

    @Override
    public boolean insert(Pays objet) {
        return false;
    }

    @Override
    public boolean update(Pays object) {
        return false;
    }

    @Override
    public boolean delete(Pays object) {
        return false;
    }

    public ArrayList<Pays> getByContinent(Continent continent) {
        ArrayList<Pays> paysListe = new ArrayList<>();
        String sqlRequest = "SELECT ID_PAYS, NOM_PAYS, C.ID_CONTINENT, NOM_CONTINENT FROM PAYS P JOIN CONTINENT C ON P.ID_CONTINENT = C.ID_CONTINENT WHERE C.ID_CONTINENT = ?";
        try (PreparedStatement preparedStatement=connexion.prepareStatement(sqlRequest)) {
            preparedStatement.setInt(1, continent.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                paysListe.add(new Pays(resultSet.getInt(1), resultSet.getString(2),new Continent(resultSet.getInt(3), resultSet.getString(4))));
            }
            resultSet.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return paysListe;
    }
}
