package fr.r.rest_sdbm.dao;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;

import java.sql.Connection;

public class SqlServerConnect {
    private static Connection sqlServerConnection;
    private SqlServerConnect() {

    }

    public static Connection getInstance(){
        if (sqlServerConnection == null) {
            try {
                SQLServerDataSource ds = new SQLServerDataSource();
                ds.setServerName("localhost");
                ds.setPortNumber(1499);
                ds.setDatabaseName("SDBM");
                ds.setIntegratedSecurity(false);
                ds.setEncrypt(false);
                ds.setUser("sa");
                ds.setPassword("azerty@123456");
                sqlServerConnection = ds.getConnection();
                System.out.println("Connexion établie ! ");
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
        return sqlServerConnection;
    }

}
