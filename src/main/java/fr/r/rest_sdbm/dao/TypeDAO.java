package fr.r.rest_sdbm.dao;


import fr.r.rest_sdbm.metier.TypeBiere;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class TypeDAO extends DAO<TypeBiere, TypeBiere, Integer> {
    @Override
    public TypeBiere getByID(Integer id) {
        String sqlRequest = "SELECT ID_TYPE, NOM_TYPE FROM TYPEBIERE where ID_TYPE = ?";
        try (PreparedStatement preparedStatement = connexion.prepareStatement(sqlRequest, Statement.RETURN_GENERATED_KEYS)){
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return new TypeBiere(resultSet.getInt(1), resultSet.getString(2));
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<TypeBiere> getAll() {
        ArrayList<TypeBiere> liste = new ArrayList<>();
        String sqlRequest = "SELECT ID_TYPE, NOM_TYPE FROM TYPEBIERE";
        try (PreparedStatement preparedStatement=connexion.prepareStatement(sqlRequest, Statement.RETURN_GENERATED_KEYS)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                liste.add(new TypeBiere(resultSet.getInt(1), resultSet.getString(2)));
            }
            resultSet.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return liste;
    }

    @Override
    public ArrayList<TypeBiere> getLike(TypeBiere objet) {
        return null;
    }

    @Override
    public boolean insert(TypeBiere objet) {
        return false;
    }

    @Override
    public boolean update(TypeBiere object) {
        return false;
    }

    @Override
    public boolean delete(TypeBiere object) {
        return false;
    }
}
