package fr.r.rest_sdbm;

import fr.r.rest_sdbm.metier.Couleur;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.ws.rs.core.MediaType;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.util.Arrays;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CouleurResourceTest {

    static int id;

    @Test
    @Order(1)
    void getAll() {
        given().get("/api/couleurs");

        Couleur[] couleurs = given()
                .get("/api/couleurs")
                .then()
                .contentType(MediaType.APPLICATION_JSON)
                .statusCode(HttpServletResponse.SC_OK)
                .extract().as(Couleur[].class);

        assert Arrays.stream(couleurs).toList().contains(new Couleur(2, "Brune"));

    }

    @Test
    @Order(2)
    void getById() {
        given().get("api/couleurs/1")
                .then()
                .contentType(MediaType.APPLICATION_JSON)
                .statusCode(HttpServletResponse.SC_OK)
                .extract().as(Couleur.class).equals(new Couleur(1, "Blonde"));
    }

    @Test
    @Order(4)
    void update() {
    }

    @Test
    @Order(3)
    void insert() {
        Couleur couleur = new Couleur(0, "Rose");
        Couleur newCouleur = given().contentType(MediaType.APPLICATION_JSON)
                .body(couleur)
                .when()
                .post("/api/couleurs")
                .then()
                .statusCode(HttpServletResponse.SC_CREATED)
                .extract().as(Couleur.class);
        id = newCouleur.getId();
        System.out.println("id de la nouvelle couleur ajoutée: "+id);
        assert newCouleur.getId() != 0;
    }

    @Test
    @Order(6)
    void delete() {
        given().when()
                .delete("/api/couleurs/"+id)
                .then()
                .statusCode(HttpServletResponse.SC_NO_CONTENT);
    }

    @Test
    @Order(5)
    void deleteFalse() {
        given().when()
                .delete("/api/couleurs/1")
                .then()
                .statusCode(HttpServletResponse.SC_BAD_REQUEST);
    }
}